import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import { User } from '../../models/user';
import {environment} from '../../../../environments/environment';


@Injectable()
export class CrudService {

  // @ts-ignore
  private prefix: string = environment.url;
  private user: User | any;
  private headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  get(url: string = '', params: any = {}) {
    this.user = JSON.parse(localStorage.getItem('user'));
    let param = '';
    for (const key in params) {
      const value = params[key];
      if (param == '') {
        param += `?${key}=${value}`;
      } else {
        param += `&${key}=${value}`;
      }
    }
    this.headers = new HttpHeaders({
      token: this.user.token,
      ContentType: 'application/json'
    });
    return this.http.get(this.prefix + url + param, {headers: this.headers});
  }

  post(url: string = '', params: any = {}, body: any = {}) {
    this.user = JSON.parse(localStorage.getItem('user'));
    let param = '';
    for (const key in params) {
      const value = params[key];
      if (param == '') {
        param += `?${key}=${value}`;
      } else {
        param += `&${key}=${value}`;
      }
    }
    this.headers = new HttpHeaders({
      token: this.user.token,
      ContentType: 'application/json'
    });
    return this.http.post(this.prefix + url + param, body, {headers: this.headers});
  }

  put(url: string = '', params: any = {}, body: any = {}) {
    this.user = JSON.parse(localStorage.getItem('user'));
    let param = '';
    for (const key in params) {
      const value = params[key];
      if (param == '') {
        param += `?${key}=${value}`;
      } else {
        param += `&${key}=${value}`;
      }
    }
    this.headers = new HttpHeaders({
      token: this.user.token,
      ContentType: 'application/json'
    });
    return this.http.put(this.prefix + url + param, body, {headers: this.headers});
  }

  authPost(url: string = '', params: any = {}, body: any = {}) {
    let param = '';
    for (const key in params) {
      const value = params[key];
      if (param == '') {
        param += `?${key}=${value}`;
      } else {
        param += `&${key}=${value}`;
      }
    }
    return this.http.post(this.prefix + url + param, body);
  }

}
