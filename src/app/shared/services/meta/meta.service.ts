import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class MetaService {

  constructor(private title: Title,
              private meta: Meta) { }

  update(meta: any) {
    if (meta.title) {
      this.title.setTitle(meta.title);
      this.meta.updateTag({name: 'title', content: meta.title});
      this.meta.updateTag({name: 'og:title', content: meta.title});
    }
    if (meta.description) {
      this.meta.updateTag({name: 'description', content: meta.description});
      this.meta.updateTag({name: 'og:description', content: meta.description});
    }
    if (meta.image) {
      this.meta.updateTag({name: 'image', content: meta.image});
      this.meta.updateTag({name: 'og:image', content: meta.image});
    }
    if (meta.url) {
      this.meta.updateTag({name: 'url', content: meta.url});
      this.meta.updateTag({name: 'og:url', content: meta.url});
    }

  }
}
