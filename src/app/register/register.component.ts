import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrudService } from '../shared/services/crud/crud.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    form: FormGroup;

    constructor(private router: Router,
                private crud: CrudService,
                private fb: FormBuilder,
                private toastr: ToastrService) {
      this.form = this.fb.group({
        name: ['', Validators.required],
        login: ['', Validators.required],
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required],
      });
    }

    ngOnInit() {
    }

    onRegister(form: any) {
      if (form.invalid) {
        this.toastr.warning('Login form invalid!');
      } else {
        const name = form.value.name;
        const login = form.value.login.replace(/\s/g, '');
        const password = form.value.password.replace(/\s/g, '');
        const confirmPassword = form.value.confirmPassword.replace(/\s/g, '');
        this.crud.authPost('users/register',{}, {name, login, password, confirmPassword})
          .subscribe((res: any) => {
            console.log(res);
            this.router.navigate(['/dashboard']);
          }, (error: any) => {
            this.toastr.error(error.error.message);
          });
      }
    }
}
