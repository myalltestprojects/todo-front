import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { CrudService } from '../../shared/services/crud/crud.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  loader: boolean;
  tasks: any;
  text: string;
  form: FormGroup;

  constructor(private crud: CrudService,
              private router: Router,
              private fb: FormBuilder,
              private toastr: ToastrService) {
    this.form = this.fb.group({
      text: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getTasks();
  }

  keyuptext(key) {
    if(key.keyCode == 13) {
      this.createTask(this.form);
    }
  }

  getTasks() {
    this.loader = true;
    this.crud.get('users/tasks').subscribe((res: any) => {
      this.tasks = res;
      this.loader = false;
    }, (error: any) => {
      this.toastr.error(error.message);
      this.loader = false;
    });
  }

  createTask(form: any) {
    if (form.invalid) {
      this.toastr.warning('Form invalid!');
    } else {
      const text = form.value.text;
      this.crud.post('users/tasks',{}, {text})
        .subscribe((res: any) => {
          form.value.text = '';
          this.text = '';
          this.getTasks();
        }, (error: any) => {
          this.toastr.warning(error.message);
        });
    }
  }
  taskUpdate(task: any) {
    task.completed = !task.completed;
    this.crud.put('users/tasks',{}, task)
    .subscribe((res: any) => {

    }, (error: any) => {
      this.toastr.warning(error.message);
    });
  }
}
