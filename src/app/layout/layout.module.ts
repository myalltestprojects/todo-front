import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatTableModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatCardModule,
} from '@angular/material';

import { TopnavComponent } from './components/topnav/topnav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        FlexLayoutModule,
        MatCardModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        MatTableModule,
        MatFormFieldModule,
        MatPaginatorModule,
        CommonModule,
    ],
    declarations: [LayoutComponent, TopnavComponent]
})
export class LayoutModule {}
