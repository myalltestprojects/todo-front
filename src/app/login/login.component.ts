import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../shared/models/user';
import { CrudService } from '../shared/services/crud/crud.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    user: User | any;
    hide = true;
    form: FormGroup;

    constructor(private router: Router,
                private crud: CrudService,
                private fb: FormBuilder,
                private toastr: ToastrService) {
      this.form = this.fb.group({
        login: ['', Validators.required],
        password: ['', Validators.required],
      });
    }

    ngOnInit() {
    }

    onLogin(loginForm: any) {
      if (loginForm.invalid) {
        this.toastr.warning('Login form invalid!');
      } else {
        const login = loginForm.value.login.replace(/\s/g, '');
        const password = loginForm.value.password.replace(/\s/g, '');
        this.crud.authPost('users/auth',{}, {login, password})
          .subscribe((res: any) => {
            if (res.error) {
              this.toastr.error(res.text, 'Error');
            } else {
              this.user = res;
              localStorage.setItem('logged', 'true');
              localStorage.setItem('user', JSON.stringify(this.user));
              this.router.navigate(['/dashboard']);
            }
          }, (error: any) => {
            this.toastr.error(error.error.message);
          });
      }
    }
}
